%{
    #include <stdio.h>
    #include <stdlib.h>
    #include "codeASM.h"
    #include "attrib.h"
    #include "bison2.tab.h" /* token type definitions from .y file */

    extern char* yytext;
    int yylex();
    int yyerror();
%}

%union {
 char varname[10];
 char strCode[250];
}

%token START
%token DATA_START
%token DATA_ENDS
%token CODE_START
%token CODE_END
%token INT
%token ID
%token SEMICOLON
%%

program : segment_date corp_program
        ;
corp_program : inceput_program sfarsit_program
             ;
inceput_program : CODE_START
                    {
                        printf("%s",TEXTSEGINCEPUT);
                        printf("%s",TEXTSEGMIJLOC);
                    }
                ;
sfarsit_program : CODE_END
                    {
                        printf("%s",TEXTSEGSFARSIT);
                        printf("%s",SFARSIT)
                    }
                ;
segment_date : inceput_segment_date declarare sfarsit_segment_date
             ;
inceput_segment_date : DATA_START
            {
                printf("%s",INCEPUT);   
                printf("%s",DATASEGINCEPUT);
            }
            ;
sfarsit_segment_date : DATA_ENDS
            {
                printf("%s",DATASEGSFARSIT);
            }
            ;
declarare : declarare var
            |
            var
            ;
var :
            INT ID SEMICOLON
            {
                printf("%s %s %d\n",yylval,"db",0);
            }
            ;
%%

int yyerror()
{
    printf("\r\nSyntax error: %s \r\n", yytext);
    //exit(3);
}

int main()
{
    return yyparse();
}
